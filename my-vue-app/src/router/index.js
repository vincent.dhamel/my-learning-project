import Vue from 'vue'
import Router from 'vue-router'
import hello from '@/components/hello'
import homepage from '@/components/homepage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'homepage',
      component: homepage
    },
    {
      path: '/hello',
      name: 'hello',
      component: hello
    }
  ]
})
