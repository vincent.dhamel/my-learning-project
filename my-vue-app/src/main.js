// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../static/glyphicons-halflings-regular.eot'
import '../static/glyphicons-halflings-regular.svg'
import '../static/glyphicons-halflings-regular.ttf'
import '../static/glyphicons-halflings-regular.woff'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
