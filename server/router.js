let url = require('url');
let fs = require('fs');
let home =  require('../controllers/home.js');
let error =  require('../controllers/404');

class router {
    static get(req,res) {
        let path = url.parse(req.url, true).pathname;
        if (/.(css)$/.test(path)) {
            res.writeHead(200, {
                'Content-Type': 'text/css'
            });
            fs.readFile('./' + path, 'utf8', (err, data) => {
                if (err) throw err;
                res.write(data, 'utf8');
                res.end();
            });
        } else {
            if (path === '/' || path === '/home') {
                let _home = new home();
                _home.get(req,res);
            } else {
                let _error = new error();
                _error.get(req,res);
            }
        }
    }
}

module.exports = router;