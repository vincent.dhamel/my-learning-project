class testData {
    constructor(){
        this._theList = {
            "GroupName": "D",
            "count": 4,
            "teams": [{
                "country": "England"
            }, {
                "country": "France"
            }, {
                "country": "Sweden"
            }, {
                "country": "Ukraine"
            }]
        };
    }

    getList(){
        return this._theList;
    }
}

module.exports = testData;
