const mainTemplate = require('../views/template-main');
const testData = require('../model/test-data');
const _ = require('underscore');
const fs = require('fs');
const index = require('./index.js');
class home {
    constructor(){
        this.data = new testData();
        this.teamList = this.data.getList();
        this.strTeam = "";
    }

    get(req,res){
        _.each(this.teamList.teams, list=>{
            this.strTeam = this.strTeam + "<li>" + list.country + "</li>";
        })
        this.strTeam = "<ul>" + this.strTeam + "</ul>";

        let content = "<p>The teams in Group " + this.teamList.GroupName + " for Euro 2012 are:</p>" + this.strTeam;
        //res.write(mainTemplate.build("Test web page on node.js", "Hello there", content));
        fs.readFile('./views/templates/dummy.html', (err, data)=>{
            res.writeHead(200, {
                'Content-Type': 'text/html'
            });
            res.write(data);
            res.end();
        });
    }
}

module.exports = home;
