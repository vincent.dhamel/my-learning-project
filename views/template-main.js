class mainTemplate {
    // static build(title, pageTitle, content) {
    //     return ['<!doctype html>',
    //         '<html lang="en">nn<meta charset="utf-8">n<title>{title}</title>',
    //         '<link rel="stylesheet" href="/assets/style.css" />n',
    //         '<h1>{pagetitle}</h1>',
    //         '<div id="content">{content}</div>nn']
    //         .join('n')
    //         .replace(/{title}/g, title)
    //         .replace(/{pagetitle}/g, pageTitle)
    //         .replace(/{content}/g, content);
    // }

    static build(title, pageTitle, content) {
        return '<html>\n' +
            '    <head>\n' +
            '        <link rel="stylesheet" href="assets/index.css">\n' +
            '        <script src="https://cdn.jsdelivr.net/npm/vue"></script>\n' +
            '    </head>\n' +
            '    <body>\n' +
            '        \n' +
            '        <div id="app">\n' +
            '            {{ message }}\n' +
            '        </div>\n' +
            '        \n' +
            '        <script src="../views/index.js"></script>\n' +
            '    </body>\n' +
            '</html>';
    }
}
module.exports = mainTemplate;
