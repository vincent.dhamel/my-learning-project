const http_IP = '127.0.0.1';
const http_port = 3000;
const http = require('http');
const router = require('./server/router');
let server = http.createServer((req, res) => {
    router.get(req, res);
}); // end server()
server.listen(http_port, http_IP);
console.log('listening to http://' + http_IP + ':' + http_port);